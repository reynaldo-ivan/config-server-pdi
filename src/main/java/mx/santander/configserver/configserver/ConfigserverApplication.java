package mx.santander.configserver.configserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer; 
import org.springframework.core.env.Environment;


@SpringBootApplication
@EnableConfigServer
public class ConfigserverApplication {
	
//	@Autowired
//    private static YAMLConfig environment;
	 

	public static void main(String[] args) {
		
		SpringApplication.run(ConfigserverApplication.class, args);
		
		YAMLConfig environment = new  YAMLConfig();
		System.out.println("puerto   "+environment.getUrl());
	}
 
}
